#include "config.h"
#include "SoftwareRaytracer.h"
#include "Scene.h"
#include <cmath>
#include <vector>
#include <thread>
#include <iostream>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <sys/time.h>


void Trace(int minx, int miny, int maxx, int maxy, SoftwareRaytracer* srt)
{
	const float znear = 0.1f;
	const float fov = M_PI / 4.0f;
	const float aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
	const glm::vec3 position(0.0f, 0.0f, 2.0f);

	while (true) {
		// Separate block
		{
			std::unique_lock<std::mutex> uniq(srt->m_mtx);
			srt->m_cond.wait(uniq);
		}

		if (srt->m_finished)
			break;
		timeval t1, t2;
		gettimeofday(&t1, NULL);

		glm::vec3 rayDir;
		Scene* scene = srt->m_scene;
		for (unsigned y = miny; y <= maxy; ++y)
			for (unsigned x = minx; x <= maxx; ++x){
				
				rayDir.x = ((1.0f * x / srt->m_width) * 2.0f - 1.0f) * aspect;
				rayDir.y = ((1.0f * y / srt->m_height) * 2.0f - 1.0f);
				rayDir.z = -znear;
				rayDir = glm::normalize(rayDir);
				
				if (scene->Intersect(position, rayDir))
				{
					srt->SetPixel(x, y, 255, 255, 255);
				}
				else {
					srt->SetPixel(x, y, 0, 0, 0);
				}
				
			}
		gettimeofday(&t2, NULL);

		std::unique_lock<std::mutex> uniq2(srt->m_mtx2);
		// std::cout << (maxx - minx + 1) * (maxy - miny + 1) << " " << ((t2.tv_sec * 1e6 + t2.tv_usec) - (t1.tv_sec * 1e6 + t1.tv_usec)) * 1e-3 << std::endl;
		srt->m_finishedThreads++;
		if (srt->m_finishedThreads == srt->m_threads.size()) {
			srt->m_condFinishedDrawing.notify_all();
		}
	}
}
void SoftwareRaytracer::Render()
{
	m_scene->Update();
	m_finishedThreads = 0;
	std::unique_lock<std::mutex> uniq(m_mtx2);
	timeval tp1, tp2;
	m_cond.notify_all();
	m_condFinishedDrawing.wait(uniq);
}
void SoftwareRaytracer::Draw()
{
	glDrawPixels(SCREEN_WIDTH, SCREEN_HEIGHT, GL_RGB, GL_UNSIGNED_BYTE,m_buffer);
}
void SoftwareRaytracer::SetPixel(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{
	int size = (y * m_width + x) * 3;
	unsigned char* px = m_buffer + size;
	*(px + 0) = r;
	*(px + 1) = g;
	*(px + 2) = b;
}
SoftwareRaytracer::SoftwareRaytracer(int width, int height):m_width(width), m_height(height), m_finished(false)
{
	m_scene = new Scene();
	m_buffer = new unsigned char[width * height * 3];
	int k = 0;
	m_threads.resize(NTHREADS);
	// for (int i = 0; i < 2; ++i)
		for (int j = 0; j < NTHREADS; ++j) {
			int minx, miny, maxx, maxy;
			minx = 0;//i * width / 2;
			maxx = width - 1;//(i + 1) * width  / 2 - 1;
			miny = j * height / NTHREADS;
			maxy = (j + 1) * height / NTHREADS - 1;
			std::thread thd = std::thread(Trace, minx, miny, maxx, maxy, this);
			m_threads[k++].swap(thd);
		}
}

SoftwareRaytracer::~SoftwareRaytracer()
{

}
