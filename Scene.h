#ifndef Scene_h
#define Scene_h
#include <glm/glm.hpp>
#include <vector>
class Object;
class Scene {
	std::vector<Object*> m_objects;
public:
	Scene();
	~Scene();
	bool Intersect(const glm::vec3&, const glm::vec3& );
	void Update();
};
#endif