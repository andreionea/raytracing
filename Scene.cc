#include "config.h"
#include "Scene.h"
#include <iostream>
#include "Object.h"
#include "Triangle.h"
#include "Rectangle.h"
Scene::Scene()
{
	
	m_objects.push_back(new Triangle
							(
								glm::vec3(-5.0f, -5.0f, 0.0f),
								glm::vec3( 5.0f, -5.0f, 0.0f),
								glm::vec3(-5.0f,  5.0f, 0.0f)
							)
						);

	m_objects.push_back(new Triangle
							(
								glm::vec3( 5.0f, -5.0f, 0.0f),
								glm::vec3( 5.0f,  5.0f, 0.0f),
								glm::vec3(-5.0f,  5.0f, 0.0f)
							)
						);

	

	m_objects.push_back(new Rectangle
							(
								glm::vec3( 5.0f,  5.0f, 0.0f),
								glm::vec3( 5.0f, -5.0f, 0.0f),
								glm::vec3(15.0f, -5.0f, -20.0f),
								glm::vec3(15.0f,  5.0f, -20.0f)
							)
						);

}
Scene::~Scene()
{
	for (auto i : m_objects)
		delete i;
}
bool Scene::Intersect(const glm::vec3& point, const glm::vec3& ray)
{
	for (auto i : m_objects) {
		if (i->Intersect(point, ray))
			return true;
	}
	return false;
}
void Scene::Update()
{
	/*
	glm::mat4 m = glm::rotate(glm::mat4(1.0f, 0.0f, 0.0f, 0.0f, 
									  0.0f, 1.0f, 0.0f, 0.0f,
									  0.0f, 0.0f, 1.0f, 0.0f,
									  0.0f, 0.0f, 0.0f, 1.0f), 1.0f, glm::vec3(0.0f, 1.0f, 1.0f));
	glm::vec4 aux;
	aux = m * glm::vec4(a, 1.0);
	a = glm::vec3(aux.x, aux.y, aux.z);
	aux = m * glm::vec4(b, 1.0);
	b = glm::vec3(aux.x, aux.y, aux.z);
	aux = m * glm::vec4(c, 1.0);
	c = glm::vec3(aux.x, aux.y, aux.z);	
	*/
} 