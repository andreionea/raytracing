#include "config.h"

#include "Rectangle.h"
#include "Triangle.h"

Rectangle::Rectangle(const glm::vec3& a,const glm::vec3& b,const glm::vec3& c,const glm::vec3& d)
{
	m_trigs[0] = new Triangle(a, b, c);
	m_trigs[1] = new Triangle(c, d, a);
}
Rectangle::~Rectangle()
{
	if (m_trigs[0])
		delete m_trigs[0];
	if (m_trigs[1])
		delete m_trigs[1];
}
bool Rectangle::Intersect(const glm::vec3& point, const glm::vec3& ray) const
{
	return (m_trigs[0] && m_trigs[1]) && 
		   (m_trigs[0]->Intersect(point, ray) || 
		    m_trigs[1]->Intersect(point, ray));
}