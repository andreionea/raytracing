CXXFLAGS += -std=c++11 -O3
CPPFLAGS += -I/opt/X11/include -I/usr/include -I/usr/local/include -I./assimp/include
SRCS = SoftwareRaytracer.cc Scene.cc main.cc Triangle.cc Rectangle.cc
LDFLAGS += -L/opt/X11/lib -L/usr/local/lib -L./assimp/lib -lassimp -lGLEW -lglut -lGL -lpthread


all: run
build: SoftwareRaytracer.o Scene.o main.o Triangle.o Rectangle.o
	$(CXX) -std=c++11 $^ $(LDFLAGS) -o main
run: build
	./main
clean:
	rm -f *.o main
depend: .depend
.depend: $(SRCS)
	rm -f ./.depend
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -MM $^>>./.depend;

include .depend
