#ifndef Rectangle_h
#define Rectangle_h
#include "Object.h"
#include <glm/glm.hpp>
class Triangle;
class Rectangle : public Object
{
	Triangle* m_trigs[2];
public:
	Rectangle(const glm::vec3&,const glm::vec3&,const glm::vec3&,const glm::vec3&);
	~Rectangle();
	virtual bool Intersect(const glm::vec3& point, const glm::vec3& ray) const;
	
};
#endif