#ifndef Object_h
#define Object_h
#include <glm/glm.hpp>

struct Object
{
public:
	virtual bool Intersect(const glm::vec3& point, const glm::vec3& ray) const = 0;
	virtual ~Object() {}
};
#endif