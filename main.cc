#include "config.h"
#include <iostream>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <sys/time.h>

#include "SoftwareRaytracer.h"
using namespace std;

SoftwareRaytracer *srt = 0;
int count_frames = 0;
float sum = 0.0f;
void display()
{
	static bool bubu = false;
	if (!srt)
	{
		glClearColor (0.0, 0.0, 0.0, 0.0);
		glClear(GL_COLOR_BUFFER_BIT);
		glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	}
	else
	{
		srt->Draw();
	}
	glutSwapBuffers();
}
void Init()
{
	SoftwareRaytracer* rt = new SoftwareRaytracer(SCREEN_WIDTH, SCREEN_HEIGHT);
	srt = rt;
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);
}
void update()
{
	if (srt){
		timeval t1, t2;
		gettimeofday(&t1, NULL);
		srt->Render();
		gettimeofday(&t2, NULL);
		sum += ((t2.tv_sec * 1e6 + t2.tv_usec) - (t1.tv_sec * 1e6 + t1.tv_usec)) * 1e-3;
		++count_frames;
		if (count_frames == 10) {
			cout << 1.0f / (sum / count_frames * 1e-3) << "fps" << endl;
			count_frames = 0;
			sum = 0.0f;
		}
		// std::cout << ((t2.tv_sec * 1e6 + t2.tv_usec) - (t1.tv_sec * 1e6 + t1.tv_usec)) * 1e-3 << std::endl;
	}
	glutPostRedisplay();
}
int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);	
	glewExperimental = GL_TRUE;
	glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	glutCreateWindow("Bubu");
	GLenum err = glewInit();
	if (GLEW_OK != err)	{
	   cerr<<"Error: "<<glewGetErrorString(err)<<endl;
	}
	else {
       cout << "It\'s all good\n";
		if (GLEW_VERSION_4_0) {
			cout << "OpenGL 4.0\n";
		
		}
    }
	Init();
	glutIdleFunc(update);
	glutDisplayFunc(display);
	glutMainLoop();
	return 0;
}