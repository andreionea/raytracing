#ifndef config_h
#define config_h
#define GLM_FORCE_RADIANS
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define NTHREADS 2
#endif