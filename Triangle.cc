#include "config.h"

#include "Triangle.h"
#include <glm/glm.hpp>
Triangle::Triangle():n(0.0, 0.0, 1.0),
			a(-1.0, -1.0, 0.0),
			b(1.0, -1.0, 0.0),
			c(0.0, 1.0, 0.0)
{
	e1 = b - a;
	e2 = c - a;
}
Triangle::Triangle(const glm::vec3 &a, const glm::vec3 &b,const glm::vec3 &c) :
			a(a), b(b), c(c)
{
	e1 = b - a;
	e2 = c - a;
	n = glm::normalize(glm::cross(e1, e2));	
}
Triangle::~Triangle() {}

bool Triangle::Intersect(const glm::vec3& point, const glm::vec3& ray) const
{
	glm::vec3 p, q, t;
	if (glm::dot(n, ray) > 0)
		return false;
	p = glm::cross(ray, e2);
	float det = glm::dot(e1, p);
	t = point - a;
	float u;
	u = glm::dot(t, p) / det;
	if (u < 0.0f || u > 1.0f)
		return false;
	q = glm::cross(t, e1);
	float v = glm::dot(ray, q) / det;
	if(v < 0.0f || u + v  > 1.0f) 
		return false;
    float T = glm::dot(e2, q) / det;
  	if(T > 0.0f) { 
    	return true;
  	}
	return false;
}