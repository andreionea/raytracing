#ifndef SoftwareRaytracer_h
#define SoftwareRaytracer_h

#include <glm/glm.hpp>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>
class Scene;
class SoftwareRaytracer
{
	std::vector<std::thread> m_threads;
	unsigned char *m_buffer;
	int m_width, m_height;
	std::condition_variable m_cond;
	std::condition_variable m_condFinishedDrawing;

	std::mutex m_mtx, m_mtx2;
	int m_finishedThreads;
	bool m_finished;
	Scene* m_scene;

	friend void Trace(int, int, int, int, SoftwareRaytracer *);
	void SetPixel(int, int, unsigned char, unsigned char, unsigned char);
public:
	void Render();
	void Draw();
	SoftwareRaytracer(int width, int height);
	~SoftwareRaytracer();
};
#endif
