#ifndef Triangle_h
#define Triangle_h

#include "Object.h"
#include <glm/glm.hpp>

class Triangle : public Object
{
	glm::vec3 n;
	glm::vec3 a;
	glm::vec3 b;
	glm::vec3 c;
	glm::vec3 e1, e2;
public:
	Triangle();
	Triangle(const glm::vec3 &, const glm::vec3 &,const glm::vec3 &);
	~Triangle();
	virtual bool Intersect(const glm::vec3& point, const glm::vec3& ray) const;
};
#endif